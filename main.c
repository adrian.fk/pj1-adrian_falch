#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

#define INPUT_SIZE 300
#define LETTERS_ALPHABET 26

//STATE INDEXES
#define STATE_EXIT 0
#define STATE_TYRE_STRAT 1
#define STATE_GEN_RADIO_CODE 2
#define STATE_COMM_DRIVER 3
#define STATE_SIM_RACE 4
#define STATE_WELCOME 5
#define STATE_MAIN_MENU 6

//ERROR INDEXES
#define ERROR_TYREASSISTANT_Q1 1
#define ERROR_TYREASSISTANT_Q2 2
#define ERROR_TYREASSISTANT_Q3 3
#define ERROR_RADIOCODE_Q1 1
#define ERROR_RADIOCODE_Q2 2
#define ERROR_WRONGRC 3
#define ERROR_WRONGMSG 4

#define ERROR_RADIOCONNECTION_DI 2
#define ERROR_RADIOCONNECTION_RAIN 3
#define ERROR_RADIOCONNECTION_NUM_LAPS 4
#define ERROR_RADIOCONNECTION_NUM_DRIVERS 5
#define ERROR_RADIOCONNECTION_NUM_RACE 6
#define ERROR_DRIVER_DRIVERNUM 7
#define ERROR_DRIVER_NAME 8
#define ERROR_DRIVER_DATEBIRTH 9
#define ERROR_DRIVER_NUMSTOP 10
#define ERROR_DRIVER_NUMLAPSTOPS 11

#define ERROR_SIM_TA 1
#define ERROR_SIM_RC 2
#define ERROR_SIM_COMM 3

#define MAX_DRIVERS 20


typedef struct {
    int length;
    char string[INPUT_SIZE];
}String;

typedef struct {
    int driverNum;
    double win_probability;
    String name;
    String dateOfBirth;
    String compound;
    int num_stops;
    int stops_indicated;
    String lap_stops;
    String driver_string;
}Driver;

typedef struct {
    int error;
    int valid_credentials;
    Driver winner;
    Driver drivers_sorted[MAX_DRIVERS];

}Simulation;

typedef struct {
    int OK;
    int error;
    int established;
    int state;
    int num_race;
    int index_degradation;
    char rain;
    int num_laps;
    int num_drivers;
    int driver_error_index;
    Driver drivers[MAX_DRIVERS];

}RadioConnection;

struct TyreAssistant {
    int OK;
    int error;
    int state;
    char raining;
    int DI;
    int SPI;
    String compound;
    int pitstops;
};

struct RadioCode {
    int OK;
    RadioConnection connection;
    int state;
    int num_driver;
    int num_race;
    String code;
    int error;
};


struct Model {
    int error;
    int error_state;
    int state;
    String input;
    struct TyreAssistant tyreAssistant;
    struct RadioCode radioCode;
    Simulation sim;
};

String setString(char input[]) {
    int i;
    String string = {0, '\0'};
    string.length = (int) strlen(input);
    for(i = 0; i < string.length; ++i) {
        string.string[i] = input[i];
    }
    return string;
}

String getString() {
    String input = {0, '\0'};
    fgets(input.string, INPUT_SIZE, stdin);
    input.length = (int) strlen(input.string) - 1;
    input.string[input.length] = '\0'; //Removing \n
    return input;
}

int stringToInt(String input) {
    int i;
    int integer = 0;
    int valid = TRUE;
    for(i = 0; i < input.length; ++i) {
        if(input.string[i] >= '0' && input.string[i] <= '9') {
            integer = (integer * 10) + (input.string[i] - '0');
        }
        else {
            valid = FALSE;
        }
    }
    if(!valid) integer = -1;
    return integer;
}

String intToString(int input) {
    char tmp[INPUT_SIZE];
    sprintf(tmp, "%d", input);
    String integer = setString(tmp);

    return integer;
}

String stringAppendChar(String string, char append) {
    if(string.length != INPUT_SIZE) {
        string.string[string.length] = append;
        string.length++;
    }
    return string;
}

String stringAppendString(String string, const char append[]) {
    int i;
    for(i = 0; i < strlen(append); i++) {
        string = stringAppendChar(string, append[i]);
    }
    return string;
}

int stringExists(String string) {
    return string.length;
}

void UI_printWelcomeMsg() {
    printf("Welcome to LS Strategist!");
}

void UI_printMainMenu() {
    printf("\n\nPlease choose an option from the menu:"
           "\n"
           "\n\t1. Tyre strategy."
           "\n\t2. Generate radio code."
           "\n\t3. Communicate with driver."
           "\n\t4. Simulate race."
           "\n\t5. Exit."
           "\n\n"
           "Selected option: ");
}

void UI_clear() {
    int i;
    for(i=0;i<50;++i) printf("\n");
}


void UI_printTyreStrat(struct TyreAssistant mode) {
    switch(mode.state) {
        case 0:
            printf("Tyre Strategy Assistant:");
            printf("\n\nIs it raining on the track? (Y/N): ");
            break;

        case 1:
            printf("\n\nWhat level of DI does the track cause on the tyre? "
                   "(0-4): ");
            break;

        case 2:
            printf("\n\nHow much do you want to prioritize the tyre speed"
                   "over durability? \n(1 - 5): ");
            break;

        default:
            break;
    }
}

void UI_printRadioCodeGenerator(struct RadioCode mode){
    switch(mode.state) {
        case 0:
            printf("\n\nCommunication code generator:");
            printf("\n\nWhat is the number of the driver whom you want to communicate?: ");
        break;

        case 1:
            printf("\n\nWhich scheduled race number?: ");
        break;

        default:
            break;
    }
}

void UI_printCommunicationInterface(struct Model model) {
    if(!model.radioCode.connection.established) {
        printf("\n\nEnter radio code: ");
    }
    else {
        if(!model.radioCode.connection.error) {
            printf("\n\nCode valid!");
        }
        printf("\n\nCommunicating with driver #%d in race #%d...", model.radioCode.num_driver,
               model.radioCode.num_race);
        printf("\n\nEnter Information:\n");
    }

}


struct Model errorHandler(struct Model m) {
    if(m.error) {
        UI_clear();
        switch (m.error_state) {
            case STATE_WELCOME:
            case STATE_MAIN_MENU:
                printf("\nThe input values should be between 1 and 5");
                break;

            case STATE_TYRE_STRAT:
                if(m.tyreAssistant.error == ERROR_TYREASSISTANT_Q1) printf("ERROR: Please enter Y or N");
                if(m.tyreAssistant.error == ERROR_TYREASSISTANT_Q2) printf("ERROR: Please enter a value between 0 and 4");
                if(m.tyreAssistant.error == ERROR_TYREASSISTANT_Q3) printf("ERROR: Please enter a value between 1 and 5");
                break;

            case STATE_GEN_RADIO_CODE:
                if(m.radioCode.error == ERROR_RADIOCODE_Q1) printf("ERROR: Please enter a value between 1 and 99");
                if(m.radioCode.error == ERROR_RADIOCODE_Q2) printf("ERROR: Please enter a value between 1 and 21");
                break;

            case STATE_COMM_DRIVER:
                if (!m.radioCode.code.length) {
                    UI_clear();
                    printf("ERROR: Encryption code not yet generated.");
                }
                if(m.radioCode.error == ERROR_WRONGRC) printf("ERROR: This code does not exist");
                if(m.radioCode.error == ERROR_WRONGMSG) {
                    switch(m.radioCode.connection.error) {
                        case ERROR_RADIOCONNECTION_RAIN:
                            printf("ERROR: The weather condition is incorrect");
                            break;

                        case ERROR_RADIOCONNECTION_NUM_LAPS:
                            printf("ERROR: Number of laps incorrect");
                            break;

                        case ERROR_RADIOCONNECTION_DI:
                            printf("ERROR: Degradation Index incorrect");
                            break;

                        case ERROR_RADIOCONNECTION_NUM_DRIVERS:
                            printf("ERROR: Number of drivers incorrect");
                            break;

                        case ERROR_RADIOCONNECTION_NUM_RACE:
                            printf("ERROR: Number of race(s) are incorrect");
                            break;

                        case ERROR_DRIVER_DRIVERNUM:
                            printf("ERROR: The driver, %s has an invalid driver number",
                                    m.radioCode.connection.drivers[m.radioCode.connection.driver_error_index - 1].name.string);
                            break;

                        case ERROR_DRIVER_NAME:
                            printf("ERROR: The driver, %s has an invalid name length",
                                   m.radioCode.connection.drivers[m.radioCode.connection.driver_error_index - 1].name.string);
                            break;

                        case ERROR_DRIVER_DATEBIRTH:
                            printf("ERROR: The driver, %s has an invalid date of birth",
                                   m.radioCode.connection.drivers[m.radioCode.connection.driver_error_index - 1].name.string);
                            break;

                        case ERROR_DRIVER_NUMSTOP:
                            printf("ERROR: The driver, %s has an invalid number of stops",
                                   m.radioCode.connection.drivers[m.radioCode.connection.driver_error_index - 1].name.string);
                            break;

                        case ERROR_DRIVER_NUMLAPSTOPS:
                            printf("ERROR: The driver, %s has %d stops, but you indicated %d",
                                   m.radioCode.connection.drivers[m.radioCode.connection.driver_error_index - 1].name.string,
                                   m.radioCode.connection.drivers[m.radioCode.connection.driver_error_index - 1].num_stops,
                                   m.radioCode.connection.drivers[m.radioCode.connection.driver_error_index - 1].stops_indicated);
                            break;

                        default:
                            break;
                    }

                }
                break;

            case STATE_SIM_RACE:
                switch (m.sim.error) {
                    case ERROR_SIM_TA:
                        printf("ERROR: You have not yet selected a tyre strategy");
                        break;

                    case ERROR_SIM_RC:
                        printf("ERROR: You have not yet generated a radio code");
                        break;

                    case ERROR_SIM_COMM:
                        printf("ERROR: You have not yet sent information to driver");
                        break;

                    default:
                        break;
                }
                break;

            default:
                break;
        }
        m.sim.error = FALSE;
        m.radioCode.connection.driver_error_index = FALSE;
        m.radioCode.connection.error = FALSE;
        m.radioCode.error = FALSE;
        m.tyreAssistant.error = FALSE;
        m.error = FALSE;
    }
    return m;
}

struct Model UI_printRaceSimulations(struct Model m) {
    int driver, i, e = 0;
    const int buffer_size = 11;
    String buffer = setString("");
    String winner = stringAppendChar(winner, '#');
    winner = stringAppendString(winner, intToString(m.sim.winner.driverNum).string);
    const int buffer_spacing = ((buffer_size - 1) / 2) - (winner.length / 2);


    printf("\n\nSimulating race...\n\nResults:\n");
    for(driver = 0; stringExists(m.sim.drivers_sorted[driver].name); driver++) {
        printf("\n%d. %s (%.2f%%)", m.sim.drivers_sorted[driver].driverNum, m.sim.drivers_sorted[driver].name.string,
                                  m.sim.drivers_sorted[driver].win_probability);
    }
    for(i = 0; i < buffer_size - 1; i++) {
        if((i < buffer_spacing - 1) || i >= (buffer_spacing + winner.length - 1)) buffer = stringAppendChar(buffer, ' ');
        else {
            buffer = stringAppendChar(buffer, winner.string[e]);
            e++;
        }
    }
    printf("\n"
           "\n     |          |"
           "\n     |          |"
           "\n     |          |"
           "\n     |          |"
           "\n     |          |"
           "\n     |          |"
           "\n       |      |"
           "\n       |      |"
           "\n     |          |"
           "\n     |%s|"
           "\n-----------------------"
           "\n\nCongratulations %s!\n\n", buffer.string, m.sim.winner.name.string);

    m.state = STATE_MAIN_MENU;
    return m;
}

struct Model UI_controller(struct Model model) {
    model = errorHandler(model);
    switch (model.state) {
        case STATE_WELCOME:
            UI_printWelcomeMsg();
        case STATE_MAIN_MENU:
            UI_printMainMenu();
            break;
        case STATE_TYRE_STRAT:
            UI_printTyreStrat(model.tyreAssistant);
            break;

        case STATE_GEN_RADIO_CODE:
            UI_printRadioCodeGenerator(model.radioCode);
            break;

        case STATE_COMM_DRIVER:
            UI_printCommunicationInterface(model);
            break;

        case STATE_SIM_RACE:
            UI_clear();
            model = UI_printRaceSimulations(model);
            UI_printMainMenu();
            break;

        default:
            break;
    }
    return model;
}

int MAINMENU_validateInput(int input) {
    int valid = 0;
    switch (input) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            valid = TRUE;
            break;

        default:
            valid = FALSE;
            break;
    }
    return valid;
}

char intToChar(int integer) {
    char character = integer + '0';
    return character;
}

struct TyreAssistant concludeTyreCode(struct TyreAssistant assistant) {
    int compound = 0;
    char tyre_type = 'C';
    char tyre_code[3] = {'\0'};


    compound = assistant.SPI - assistant.DI;
    compound = abs(compound);

    if(assistant.raining == 'Y') {
        if(compound >= 3) tyre_type = 'I';
        else {
            tyre_type = 'W';
        }
    }
    else {
        tyre_code[1] = intToChar(compound);
    }
    tyre_code[0] = tyre_type;
    assistant.compound = setString(tyre_code);

    assistant.pitstops = 1;
    if(assistant.SPI > 3) assistant.pitstops = 3;
    else if(assistant.SPI == 3) assistant.pitstops = 2;

    return assistant;
}

void UI_printTyreCode(struct TyreAssistant assistant) {
    UI_clear();
    printf("The appropriate compound is %s and %d pit stops must be made.",
            assistant.compound.string, assistant.pitstops);
}

struct Model TYRE_ASSISTANT_updateStats(struct Model m) {
    switch(m.tyreAssistant.state) {
        case 0:
            if(m.input.length == 1 && (m.input.string[0] == 'Y' || m.input.string[0] == 'N' )) {
                m.tyreAssistant.raining = m.input.string[0];
                m.tyreAssistant.state++;
            }
            else{
                m.error = TRUE;
                m.tyreAssistant.error = ERROR_TYREASSISTANT_Q1;
            }
            break;

        case 1:
            if (stringToInt(m.input) >= 0 && stringToInt(m.input) <= 4) {
                m.tyreAssistant.DI = stringToInt(m.input);
                m.tyreAssistant.state++;
            }
            else {
                m.error = TRUE;
                m.tyreAssistant.error = ERROR_TYREASSISTANT_Q2;
            }
            break;

        case 2:
            if (stringToInt(m.input) >= 1 && stringToInt(m.input) <= 5) {
                m.tyreAssistant.SPI = stringToInt(m.input);
                m.tyreAssistant = concludeTyreCode(m.tyreAssistant);
                //Handle printing of code
                UI_printTyreCode(m.tyreAssistant);
                //Handle reset of variables such that we return to main menu
                m.state = STATE_MAIN_MENU;
                m.tyreAssistant.state = 0;
                m.tyreAssistant.OK = TRUE;
            }
            else {
                m.error = TRUE;
                m.tyreAssistant.error = ERROR_TYREASSISTANT_Q3;
            }
            break;

        default:
            break;
    }

    return m;
}

struct RadioCode RC_generator_genCode(struct RadioCode radioCode) {
    String code = {0, '\0'};
    int letter = 0;
    int rounds = 0;
    char final_letter;
    rounds = radioCode.num_driver / 26;
    letter = radioCode.num_driver - (rounds * LETTERS_ALPHABET);

    if(radioCode.num_driver % LETTERS_ALPHABET) {
        final_letter = letter + 'A' - 1;
    }
    else {
        final_letter = 'Z';
    }
    code = stringAppendChar(code, final_letter);

    code = stringAppendString(code, intToString(radioCode.num_driver / radioCode.num_race).string);

    code = stringAppendString(code, intToString(rounds).string);

    radioCode.code = setString(code.string);

    return radioCode;
}

void UI_printRadioCode(struct RadioCode radio) {
    UI_clear();
    printf("CODE GENERATED: %s", radio.code.string);
}

struct Model RC_generator(struct Model m) {
    switch(m.radioCode.state) {
        case 0:
            //Driver number (1-99)
            if(stringToInt(m.input) >= 1 && stringToInt(m.input) <= 99) {
                m.radioCode.num_driver = stringToInt(m.input);
                m.radioCode.state++;
            }
            else {
                m.error = TRUE;
                m.radioCode.error = ERROR_RADIOCODE_Q1;
            }
            break;

        case 1:
            //Race number (1-21)
            if(stringToInt(m.input) >= 1 && stringToInt(m.input) <= 21) {
                m.radioCode.num_race = stringToInt(m.input);
                m.radioCode = RC_generator_genCode(m.radioCode);
                UI_printRadioCode(m.radioCode);
                m.state = STATE_MAIN_MENU;
                m.radioCode.state = 0;
                m.radioCode.OK = TRUE;
            }
            else {
                m.error = TRUE;
                m.radioCode.error = ERROR_RADIOCODE_Q2;
            }
            break;

        default:
            break;
    }
    return m;
}

RadioConnection parseRadioMsgInput(RadioConnection m, int data_type, int driver, String buffer) {
    switch (data_type) {
        case 0:
            m.num_race = stringToInt(buffer);
            break;
        case 1:
            m.index_degradation = stringToInt(buffer);
            break;

        case 2:
            m.rain = buffer.string[0];
            break;

        case 3:
            m.num_laps = stringToInt(buffer);
            break;

        case 4:
            m.num_drivers = stringToInt(buffer);
            break;

        case 5:
            buffer = stringAppendChar(buffer, '-');
            m.drivers[driver].driver_string = setString(buffer.string);
            break;
        default:
            break;
    }
    return m;
}

struct Model parseRadioMsg(struct Model m) {
    int i;
    String buffer = setString("");
    char data_sep = '|';
    int data_type = 0;
    int driver = 0;
    for(i = 0; i < m.input.length; ++i) {
        if(m.input.string[i] == data_sep){
            m.radioCode.connection = parseRadioMsgInput(m.radioCode.connection, data_type, driver, buffer);
            data_type >= 5 ? driver++ : data_type++;
            buffer = setString("");
        }
        else {
            buffer = stringAppendChar(buffer, m.input.string[i]);
        }
    }
    m.radioCode.connection = parseRadioMsgInput(m.radioCode.connection, data_type, driver, buffer);
    return m;

    /**
     *
     * for(i = 0; i < getStringLength(m.input); ++i) {
     *     if(isDataSeparator(m.input, data_sep)) {
     *         m.radio.connection = processData(m.radioCode.connection, data_type, driver, buffer);
     *         data_type >= 5 driver++ : data_type++;
     *         clearBuffer(buffer);
     *     }
     *     else {
     *         stringAppendWithChar(buffer, m.input.string[i];
     *     }
     *
     *     m.radio.connection = processData(...);
     * }
     */
}

int validateDate(String _input) {
    int valid = TRUE;
    int i;
    int day = 0, month = 0, year = 0;
    String buffer = setString("");
    char data_sep = '/';
    int data_type = 0;
    for(i = 0; i < _input.length; i++) {
        if(data_sep == _input.string[i]) {
            switch(data_type) {
                case 0:
                    day = stringToInt(buffer);
                    break;

                case 1:
                    month = stringToInt(buffer);
                    break;

                case 2:
                    year = stringToInt(buffer);
                    break;

                default:
                    break;
            }
            data_type++;
            buffer = setString("");
        }
        else {
            buffer = stringAppendChar(buffer, _input.string[i]);
        }
    }

    if(day < 1 || month < 1 || month > 12 || year > 2019 || year < 0) valid = FALSE;
    if(year % 400 == 0){
        if(month == 2 && day > 29) valid = FALSE;
    }
    else {
        if( month == 2 && day > 28) valid = FALSE;
    }
    if(month % 2) {
        if (day > 30) valid = FALSE;
    }
    else {
        if(day > 31) valid = FALSE;
    }

    return valid;
}

int validateNumLapStops(Driver driver) {
    return driver.stops_indicated == driver.num_stops;
}

Driver evaluateDriverStops(Driver driver) {
    int i;
    int num_stops_counted = 1;
    for(i = 0; i < driver.lap_stops.length; i++) {
        if('/' == driver.lap_stops.string[i]) num_stops_counted++;
    }
    driver.stops_indicated = num_stops_counted;

    return driver;
}

RadioConnection validateRadioMsgValues(RadioConnection m) {
    int driver = 0;
    if (m.num_race < 1 || m.num_race > 21) {
        m.error = ERROR_RADIOCONNECTION_NUM_RACE;
    }
    else {
        if (m.index_degradation < 0 || m.index_degradation > 4) {
            m.error = ERROR_RADIOCONNECTION_DI;
        }
        else {
            if (m.rain != 'Y' && m.rain != 'y' && m.rain != 'N' && m.rain != 'n') {
                m.error = ERROR_RADIOCONNECTION_RAIN;
            }
            else {
                if (m.num_laps < 0 || m.num_laps > 10) {
                    m.error = ERROR_RADIOCONNECTION_NUM_LAPS;
                }
                else {
                    if (m.num_drivers > 20 || m.num_drivers < 2) {
                        m.error = ERROR_RADIOCONNECTION_NUM_DRIVERS;
                    }
                    else {
                        //Validating driver data
                        for(driver = 0; stringExists(m.drivers[driver].name)
                        && !m.driver_error_index; driver++) {
                            Driver d = m.drivers[driver];
                            if(d.driverNum < 1 || d.driverNum > 99) {
                                m.error = ERROR_DRIVER_DRIVERNUM;
                                m.driver_error_index = driver + 1;
                            }
                            else {
                                if(d.name.length < 1 || d.name.length > 25) {
                                    m.error = ERROR_DRIVER_NAME;
                                    m.driver_error_index = driver + 1;
                                }
                                else {
                                    if(!validateDate(stringAppendChar(d.dateOfBirth, '/'))) {
                                        m.error = ERROR_DRIVER_DATEBIRTH;
                                        m.driver_error_index = driver + 1;
                                    }
                                    else {
                                        if(d.num_stops < 0 || d.num_stops > 11) {
                                            m.error = ERROR_DRIVER_NUMSTOP;
                                            m.driver_error_index = driver + 1;
                                        }
                                        else {
                                            m.drivers[driver] = evaluateDriverStops(d);
                                            if(!validateNumLapStops(m.drivers[driver])) {
                                                m.error = ERROR_DRIVER_NUMLAPSTOPS;
                                                m.driver_error_index = driver + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return m;
}

Driver processDriverData(Driver driver, int data_type, String buffer) {
    switch(data_type) {
        case 0:
            driver.driverNum = stringToInt(buffer);
            break;

        case 1:
            driver.name = setString(buffer.string);
            break;

        case 2:
            driver.dateOfBirth = setString(buffer.string);
            break;

        case 3:
            driver.compound = setString(buffer.string);
            break;

        case 4:
            driver.num_stops = stringToInt(buffer);
            break;

        case 5:
            driver.lap_stops = setString(buffer.string);
            break;

        default:
            break;
    }
    return driver;
}

RadioConnection parseRadioMsgDrivers(RadioConnection m) {
    int i, driver;
    String buffer;
    char data_sep[2] = {'-', '|'};
    int data_type;
    for(driver = 0; stringExists(m.drivers[driver].driver_string); ++driver) {
        data_type = 0;
        buffer = setString("");
        for (i = 0; i < m.drivers[driver].driver_string.length; ++i) {
            if (m.drivers[driver].driver_string.string[i] == data_sep[0] ||
                    m.drivers[driver].driver_string.string[i] == data_sep[1]) {
                //Switch to put correct data into correct variables
                m.drivers[driver] = processDriverData(m.drivers[driver], data_type, buffer);
                data_type++;
                buffer = setString("");
            } else {
                buffer = stringAppendChar(buffer, m.drivers[driver].driver_string.string[i]);
            }
        }
    }
    return m;
}

struct Model createRadioTrans(struct Model m) {
    m = parseRadioMsg(m);
    m.radioCode.connection = parseRadioMsgDrivers(m.radioCode.connection);
    m.radioCode.connection = validateRadioMsgValues(m.radioCode.connection);
    if(m.radioCode.connection.error) {
        m.error = TRUE;
        m.radioCode.error = ERROR_WRONGMSG;
    }
    else {
        UI_clear();
        printf("Information validated and sent successfully!");
        m.state = STATE_MAIN_MENU;
        m.radioCode.connection.OK = TRUE;
    }
    return m;
}

struct Model radioInterface(struct Model m) {
    if(m.radioCode.code.length) {
        //conditional for if we are communicating or not
        if(!m.radioCode.connection.established) {
            if (!strcmp(m.input.string, m.radioCode.code.string)) {
                //UI_clear();
                m.radioCode.connection.established = TRUE;
            } else {
                if (!strcmp(m.input.string, "exit")) {
                    UI_clear();
                    m.state = STATE_MAIN_MENU;
                } else {
                    m.error = TRUE;
                    m.radioCode.error = ERROR_WRONGRC;
                }
            }
        }
        else {
            m = createRadioTrans(m);
        }
    }
    return m;
}

Simulation validateSimCredentials(struct Model m) {
    m.sim.valid_credentials = TRUE;
    if(!m.tyreAssistant.OK) {
        m.sim.error = ERROR_SIM_TA;
        m.sim.valid_credentials = FALSE;
    }
    else {
        if(!m.radioCode.OK) {
            m.sim.error = ERROR_SIM_RC;
            m.sim.valid_credentials = FALSE;
        }
        else {
            if(!m.radioCode.connection.OK) {
                m.sim.error = ERROR_SIM_COMM;
                m.sim.valid_credentials = FALSE;
            }
        }
    }

    return m.sim;
}

void UI_printExitMsg() {
    printf("\n\nThank you for using the program!"
           "\nGood luck on your next race!");
}

RadioConnection sortDrivers_swap(RadioConnection m, int x) {
    Driver tmp = m.drivers[x];
    m.drivers[x] = m.drivers[x + 1];
    m.drivers[x + 1] = tmp;
    return m;
}

Simulation concludeSimWinner_swap(Simulation m, int x) {
    Driver tmp = m.drivers_sorted[x];
    m.drivers_sorted[x] = m.drivers_sorted[x + 1];
    m.drivers_sorted[x + 1] = tmp;
    return m;
}

Driver concludeSimWinner(Simulation m, RadioConnection rc) {
    int i, driver;

    for(i = 0; i < rc.num_drivers - 1; i++) {
        for(driver = 0; driver < rc.num_drivers - i - 1; driver++)
            if(m.drivers_sorted[driver].win_probability < m.drivers_sorted[driver + 1].win_probability)
                m = concludeSimWinner_swap(m, driver);

    }
    return m.drivers_sorted[0];
}

Simulation sortDrivers(Simulation sim, RadioConnection m) {
    int i, driver;

    for(i = 0; i < m.num_drivers - 1; i++) {
        for(driver = 0; driver < m.num_drivers - i - 1; driver++)
            if(m.drivers[driver].driverNum > m.drivers[driver + 1].driverNum)
                m = sortDrivers_swap(m, driver);

    }
    for(i = 0; i < MAX_DRIVERS; i++) {
        sim.drivers_sorted[i] = m.drivers[i];
    }
    return sim;
}

double calcWinProb(Driver drvr, struct Model m, int _drivers_infront) {
    double prob;
    float compound = 0.0;
    float num_stops = m.tyreAssistant.pitstops - drvr.num_stops;
    float drivers_infront = _drivers_infront;
    float total_num_drivers = m.radioCode.connection.num_drivers;
    float luck = 10;

    while(luck == 8 || luck == 9 || luck == 10) {
        luck = rand() * 10;
    }

    switch(m.tyreAssistant.compound.string[0]) {
        case 'C':
            if(drvr.compound.string[0] == 'I') compound = 1;
            else
            if(drvr.compound.string[0] == 'W') compound = 2;
            break;

        case 'I':
            if(drvr.compound.string[0] == 'C' || drvr.compound.string[0] == 'W') compound = 1;
            break;

        case 'W':
            if(drvr.compound.string[0] == 'I') compound = 1;
            else
            if(drvr.compound.string[0] == 'C') compound = 2;
            break;

        default:
            break;
    }

    prob = (100 - sqrt(pow(compound, 2.0)+pow(num_stops, 2.0)) - cos(((drivers_infront / total_num_drivers)*100.0 *(2.0/3.0))
            + ((1.0/3.0)*luck)) * 40.0);
    if(prob >= 100.00) prob = 100.00;

    return prob;
}

Simulation simulateRace(struct Model m) {
    int driver;

    m.sim = sortDrivers(m.sim, m.radioCode.connection);
    for(driver = 0; stringExists(m.sim.drivers_sorted[driver].name); driver++) {
        m.sim.drivers_sorted[driver].win_probability = calcWinProb(m.sim.drivers_sorted[driver], m, driver);
    }
    m.sim.winner = concludeSimWinner(m.sim, m.radioCode.connection);


    return m.sim;
}

struct Model interpretInput(struct Model m) {
    int int_input = 0;
    m.error_state = m.state;

    switch (m.state) {
        case STATE_WELCOME:
            m.state++;
        case STATE_MAIN_MENU:
            int_input = stringToInt(m.input);
            if(MAINMENU_validateInput(int_input)) {
                m.state = int_input;
                if(m.state == 5){
                    m.state = STATE_EXIT;
                    UI_printExitMsg();
                }
                else UI_clear();
                if((m.state == 3) && !stringExists(m.radioCode.code)) {
                    m.error = TRUE;
                    m.error_state = m.state;
                    m.state = STATE_MAIN_MENU;
                }
                m.sim = validateSimCredentials(m);
                if(m.state == STATE_SIM_RACE) {
                    if (!m.sim.valid_credentials) {
                        m.error = TRUE;
                        m.error_state = m.state;
                        m.state = STATE_MAIN_MENU;
                    }
                    else {
                        m.sim = simulateRace(m);
                    }
                }
            }
            else {
                m.error = TRUE;
            }
            break;

        case STATE_TYRE_STRAT:
            m = TYRE_ASSISTANT_updateStats(m);
            break;

        case STATE_GEN_RADIO_CODE:
            m = RC_generator(m);
            break;

        case STATE_COMM_DRIVER:
            m = radioInterface(m);
            break;

        default:
            break;
    }
    return m;
}

struct Model initModel() {
    struct Model model;
    model.state = STATE_WELCOME;
    model.tyreAssistant.state = 0;
    model.radioCode.state = 0;
    model.radioCode.code.length = 0;
    model.error = FALSE;
    model.radioCode.error = FALSE;
    model.tyreAssistant.error = FALSE;
    model.radioCode.connection.established = FALSE;
    model.radioCode.connection.state = FALSE;
    model.radioCode.connection.error = FALSE;
    model.radioCode.connection.error = FALSE;
    model.radioCode.connection.driver_error_index = FALSE;
    model.tyreAssistant.OK = FALSE;
    model.radioCode.OK = FALSE;
    model.sim.error = FALSE;
    model.sim.valid_credentials = FALSE;
    return model;
}

int main() {
    struct Model model = initModel();
    while(model.state) {
        model = UI_controller(model);
        model.input = getString();
        model = interpretInput(model);
    }
    return 0;
}